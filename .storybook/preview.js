/*
  App-wide decorators and paramters for storybook are added here
*/
import React from 'react'
import {ThemeProvider} from 'styled-components'
import styleGuidelines from '../src/style-guidelines'

export const decorators = [
  Story => (
    <ThemeProvider theme={styleGuidelines}>
      <Story />
    </ThemeProvider>
  )
]

export const parameters = {
  actions: {argTypesRegex: '^on[A-Z].*'}
}
