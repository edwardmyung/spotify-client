import React from 'react'
import ReactDOM from 'react-dom'
import styleGuidelines from './style-guidelines'
import {ThemeProvider} from 'styled-components'
import {BrowserRouter as Router} from 'react-router-dom'
import App from './App'
import {StoreProvider} from './context/store'

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={styleGuidelines}>
      <StoreProvider>
        <Router>
          <App />
        </Router>
      </StoreProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
)

// module.hot.accept()
