export const fetchPlayerContext = (accessToken: string): Request => {
  /*
    Note: this request is not in use in this project, as the API is unstable and liable to change (Nov 2020)
    Ideally, this request is used to fetch the current context and set it on a store, from which the player will know 
    which location to start from
  */
  return new Request(`https://api.spotify.com/v1/me/player/currently-playing`, {
    method: 'GET',
    headers: new Headers({
      Authorization: `Bearer ${accessToken}`
    })
  })
}

export const playTrack = (accessToken: string, deviceId: string, body: BodyInit): Request => {
  return new Request(`https://api.spotify.com/v1/me/player/play?device_id=${deviceId}`, {
    method: 'PUT',
    body,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`
    }
  })
}

export const pauseTrack = (accessToken: string, deviceId: string): Request => {
  return new Request(`https://api.spotify.com/v1/me/player/pause?device_id=${deviceId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`
    }
  })
}
