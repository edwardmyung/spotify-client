export const fetchAuthedUserPlaylists = (accessToken: string, limit: number, offset: number): Request => {
  return new Request(`https://api.spotify.com/v1/me/playlists?limit=${limit}&offset=${offset}`, {
    method: 'GET',
    headers: new Headers({
      Authorization: `Bearer ${accessToken}`
    })
  })
}

export const fetchPlaylist = (accessToken: string, playlistId: string): Request => {
  return new Request(`https://api.spotify.com/v1/playlists/${playlistId}`, {
    method: 'GET',
    headers: new Headers({
      Authorization: `Bearer ${accessToken}`
    })
  })
}
