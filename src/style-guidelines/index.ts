import * as constants from './constants'
import app from './app'

export default {
  constants,
  app
}
