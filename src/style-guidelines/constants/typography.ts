import {map} from 'lodash'
import {css, FlattenSimpleInterpolation} from 'styled-components'
import breakpoints from './breakpoints'

export enum FontWeight {
  bold = 700,
  regular = 400
}

type TGelFonts = Record<string, TGelFont>
type TGelFont = TGelFontSubType[]

interface TGelFontSubType {
  fontSize: number
  lineHeight: number
}

const GEL_FONTS: TGelFonts = {
  canon: [
    {
      fontSize: 28,
      lineHeight: 1.3
    },
    {
      fontSize: 32,
      lineHeight: 1.3
    },
    {
      fontSize: 44,
      lineHeight: 1.3
    }
  ],
  trafalgar: [
    {
      fontSize: 20,
      lineHeight: 1.3
    },
    {
      fontSize: 24,
      lineHeight: 1.3
    },
    {
      fontSize: 32,
      lineHeight: 1.3
    }
  ],
  paragon: [
    {
      fontSize: 20,
      lineHeight: 1.3
    },
    {
      fontSize: 22,
      lineHeight: 1.3
    },
    {
      fontSize: 28,
      lineHeight: 1.3
    }
  ],
  doublePica: [
    {
      fontSize: 20,
      lineHeight: 1.3
    },
    {
      fontSize: 20,
      lineHeight: 1.3
    },
    {
      fontSize: 24,
      lineHeight: 1.3
    }
  ],
  greatPrimer: [
    {
      fontSize: 18,
      lineHeight: 1.3
    },
    {
      fontSize: 18,
      lineHeight: 1.3
    },
    {
      fontSize: 20,
      lineHeight: 1.3
    }
  ],
  bodyCopy: [
    {
      fontSize: 15,
      lineHeight: 1.3
    },
    {
      fontSize: 16,
      lineHeight: 1.3
    },
    {
      fontSize: 16,
      lineHeight: 1.3
    }
  ],
  pica: [
    {
      fontSize: 15,
      lineHeight: 1.3
    },
    {
      fontSize: 16,
      lineHeight: 1.3
    },
    {
      fontSize: 16,
      lineHeight: 1.3
    }
  ],
  longPrimer: [
    {
      fontSize: 15,
      lineHeight: 1.3
    },
    {
      fontSize: 15,
      lineHeight: 1.3
    },
    {
      fontSize: 14,
      lineHeight: 1.3
    }
  ],
  brevier: [
    {
      fontSize: 14,
      lineHeight: 1.3
    },
    {
      fontSize: 14,
      lineHeight: 1.3
    },
    {
      fontSize: 13,
      lineHeight: 1.3
    }
  ],
  minion: [
    {
      fontSize: 12,
      lineHeight: 1.3
    },
    {
      fontSize: 12,
      lineHeight: 1.3
    },
    {
      fontSize: 12,
      lineHeight: 1.3
    }
  ]
}

const typographyCss: Record<string, (fontWeight: FontWeight) => FlattenSimpleInterpolation> = {}

const textCrop = (lineHeightEm: number, topAdjustment: number, bottomAdjustment: number) => {
  const topCrop = 14
  const bottomCrop = 9.3
  const cropFontSize = 44
  const cropLineHeight = 1.45
  const dynamicTopCrop = (topCrop + (lineHeightEm - cropLineHeight) * (cropFontSize / 2)) / cropFontSize
  const dynamicBottomCrop = (bottomCrop + (lineHeightEm - cropLineHeight) * (cropFontSize / 2)) / cropFontSize

  return css`
    line-height: ${lineHeightEm};
    &::before,
    &::after {
      content: '';
      display: block;
      height: 0;
      width: 0;
    }

    &::before {
      margin-bottom: -${dynamicTopCrop + topAdjustment}em;
    }
    &::after {
      margin-top: -${dynamicBottomCrop + bottomAdjustment}em;
    }
  `
}

map(GEL_FONTS, (v: TGelFont, k: string) => {
  typographyCss[k] = (fontWeight: FontWeight): FlattenSimpleInterpolation => css`
    @media screen and (max-width: ${breakpoints.xs}px) {
      font-size: ${v[0].fontSize}px;
      line-height: ${v[0].lineHeight}em;
      /* ${textCrop(v[0].lineHeight, 0, 0)} */
    }

    @media screen and (max-width: ${breakpoints.sm}px) {
      font-size: ${v[1].fontSize}px;
      line-height: ${v[1].lineHeight}em;
      /* ${textCrop(v[1].lineHeight, 0, 0)} */
    }

    font-size: ${v[2].fontSize}px;
    line-height: ${v[2].lineHeight}em;
    font-weight: ${fontWeight};
    /* ${textCrop(v[2].lineHeight, 0, 0)} */
  `
})

export default {
  css: typographyCss,
  fontWeights: FontWeight
}
