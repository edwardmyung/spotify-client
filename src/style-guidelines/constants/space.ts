enum scale {
  two = 2,
  four = 4,
  eight = 8,
  twelve = 12,
  sixteen = 16,
  twentyFour = 24,
  thirtyTwo = 32,
  forty = 40,
  fortyEight = 48,
  fiftySix = 56,
  sixtyFour = 64
}

const css = {
  inset: {
    xs: `padding: ${scale.four}px;`,
    s: `padding: ${scale.eight}px;`,
    m: `padding: ${scale.sixteen}px;`,
    l: `padding: ${scale.thirtyTwo}px;`,
    xl: `padding: ${scale.fortyEight}px;`
  },
  insetSquish: {
    s: `padding: ${scale.four}px ${scale.eight}px;`,
    m: `padding: ${scale.twelve}px ${scale.sixteen}px;`,
    l: `padding: ${scale.sixteen}px ${scale.thirtyTwo}px;`
  },
  insetStretch: {
    s: `padding: ${scale.sixteen}px ${scale.eight}px;`,
    m: `padding: ${scale.twentyFour}px ${scale.sixteen}px;`
  },
  stack: {
    xxs: `margin-bottom: ${scale.two}px;`,
    xs: `margin-bottom: ${scale.four}px;`,
    s: `margin-bottom: ${scale.eight}px;`,
    m: `margin-bottom: ${scale.sixteen}px;`,
    l: `margin-bottom: ${scale.thirtyTwo}px;`,
    xl: `margin-bottom: ${scale.sixtyFour}px;`
  },
  inline: {
    left: {
      xxs: `margin-left: ${scale.two}px;`,
      xs: `margin-left: ${scale.four}px;`,
      s: `margin-left: ${scale.eight}px;`,
      m: `margin-left: ${scale.sixteen}px;`,
      l: `margin-left: ${scale.thirtyTwo}px;`,
      xl: `margin-left: ${scale.sixtyFour}px;`
    },
    right: {
      xxs: `margin-right: ${scale.two}px;`,
      xs: `margin-right: ${scale.four}px;`,
      s: `margin-right: ${scale.eight}px;`,
      m: `margin-right: ${scale.sixteen}px;`,
      l: `margin-right: ${scale.thirtyTwo}px;`,
      xl: `margin-right: ${scale.sixtyFour}px;`
    }
  }
}


export default {
  scale,
  css
}
