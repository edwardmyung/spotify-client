export default {
  white: '#FFFFFF',
  black: '#000000',
  brand: {
    main: '#1db954',
    hover: '#4AC776'
  },
  grays: {
    100: '#040404',
    95: '#131313',
    90: '#242424',
    80: '#2D2D2D',
    70: '#494949',
    65: '#505050',
    60: '#5A5A5A',
    30: '#AAAAAA'
  }
}
