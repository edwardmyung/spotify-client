import {colors, space} from './constants'
import {TButtonSize, TButtonTheme} from '../components/atoms/buttons/types'
import {EnumIconButtonFill} from '../components/atoms/buttons/IconButton'

export default {
  accessibilityOutline: `box-shadow: 0 0 0 2px ${colors.grays[30]};`,
  buttons: {
    sizes: {
      [TButtonSize.small]: {
        insetSquish: space.css.insetSquish.s
      },
      [TButtonSize.medium]: {
        insetSquish: space.css.insetSquish.m
      },
      [TButtonSize.large]: {
        insetSquish: space.css.insetSquish.l
      }
    },
    textButton: {
      colors: {
        [TButtonTheme.primary]: {
          inactiveColor: colors.grays[30],
          activeColor: colors.white
        }
      }
    },
    pillButton: {
      colors: {
        [TButtonTheme.primary]: {
          background: null,
          hoverBackground: null,
          activeBackground: colors.grays[80]
        },
        [TButtonTheme.brand]: {
          background: colors.brand.main,
          hoverBackground: colors.brand.hover,
          activeBackground: null
        }
      }
    },
    circleButton: {
      colors: {
        [TButtonTheme.primary]: {
          background: 'rgba(0,0,0,.7)',
          disabledBackground: 'rgba(0,0,0,0.3)'
        },
        [TButtonTheme.brand]: {
          background: colors.brand.main,
          disabledBackground: null
        }
      }
    },
    iconButton: {
      colors: {
        [EnumIconButtonFill.default]: {
          fill: 'grey',
          hoverFill: 'white'
        }
      }
    }
  },

  /*
   * Styles for the core app layout
   */
  layout: {
    footer: {
      background: colors.grays[80]
    },
    sidebar: {
      background: colors.grays[100],
      insetCss: space.css.inset.s,
      insetSquish: space.css.insetSquish.s
    }
  }
}
