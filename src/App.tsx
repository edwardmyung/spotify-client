import React, {useEffect} from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'

import {Body as HomeBody} from './components/pages/Home'
import {Body as BrowseBody} from './components/pages/Browse'
import {Body as CollectionBody} from './components/pages/Collection'
import {Body as PlaylistBody} from './components/pages/Playlist'
import Login from './components/pages/Login'
import AppLayout from './components/organisms/AppLayout'
import {useStore} from './context/store'
import {observer} from 'mobx-react'
import {IUserAuth} from './mobx/UiStore/types'
import {IPlaylist} from './mobx/PlaylistStore/types'
import 'simplebar/dist/simplebar.min.css'

const PrivateRoute = observer(({component, ...rest}) => {
  const Component = component
  const store = useStore()
  const isAuthenticated = !!store.ui.userAuth.access_token
  const userPlaylists: IPlaylist[] = store.playlist.userPlaylistIds.map(id => store.playlist.playlists[id])

  return (
    <AppLayout
      token={store.ui.userAuth.access_token}
      userPlaylists={userPlaylists}
      fetchUserPlaylists={() => store.playlist.fetchUserPlaylists()}
      body={
        <Route {...rest} render={props => (isAuthenticated ? <Component {...props} /> : <Redirect to='/login' />)} />
      }
    />
  )
})

/*
  Get the hash of the url
*/
const hash = window.location.hash
  .substring(1)
  .split('&')
  .reduce(function (initial, item) {
    if (item) {
      const parts = item.split('=')
      initial[parts[0]] = decodeURIComponent(parts[1])
    }
    return initial
  }, {}) as IUserAuth
window.location.hash = ''

const T = () => <div>lol</div>

const App = observer(
  (): JSX.Element => {
    const store = useStore()

    if (hash.access_token) {
      store.ui.setUserAuth(hash as IUserAuth)
    }

    useEffect(() => {
      if (store.ui.userAuth.access_token) {
        // Do all initial fetching here
        store.playlist.fetchUserPlaylists()
      }
    }, [])

    return (
      <Switch>
        <PrivateRoute exact path='/' component={HomeBody} />
        <PrivateRoute path='/browse' component={BrowseBody} />
        <PrivateRoute path='/collection' component={CollectionBody} />
        <PrivateRoute path='/playlist/:playlistId' component={PlaylistBody} />
        <Route path='/login' component={Login} />
      </Switch>
    )
  }
)

export default App
