import {makeAutoObservable} from 'mobx'
import Track from './Track'
import RootStore from '../RootStore'
import {ITrackStore, ITrack} from './types'

class TrackStore implements ITrackStore {
  rootStore: RootStore
  tracks = {} as Record<string, Track>

  constructor(rootStore: RootStore) {
    makeAutoObservable(this)
    this.rootStore = rootStore
  }

  updateTrackFromServer(json: ITrack): Track {
    const maybeTrack = this.tracks[json.id]
    json.artists.map(artist => this.rootStore.artist.updateArtistFromServer(artist))
    this.rootStore.album.updateAlbumFromServer(json.album)

    if (maybeTrack) {
      maybeTrack.updateFromJson(json)
    } else {
      const track = new Track(json)
      this.tracks[json.id] = track
    }
    return this.tracks[json.id]
  }
}

export default TrackStore
