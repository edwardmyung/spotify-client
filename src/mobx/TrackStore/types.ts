import Track from './Track'
import {IArtist} from '../ArtistStore/types'
import {IAlbum} from '../AlbumStore/types'
import {IImage} from '../types'

export interface ITrackStore {
  tracks: Record<string, Track>
}

export interface ITrack {
  album: IAlbum
  artists: IArtist[]
  available_markets: string[]
  disc_number: number
  duration_ms: number
  episode: boolean
  explicit: boolean
  external_ids: {
    isrc: string
  }
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  is_local: boolean
  name: string
  popularity: number
  preview_url: string
  track: boolean
  track_number: number
  type: string
  uri: string
}
