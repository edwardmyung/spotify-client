import {ITrack} from './types'
import {makeAutoObservable} from 'mobx'

class Track implements ITrack {
  type = 'track'
  album
  artists
  available_markets
  disc_number
  duration_ms
  episode
  explicit
  external_ids
  external_urls
  href
  id
  is_local
  name
  popularity
  preview_url
  track
  track_number
  uri

  constructor(json: ITrack) {
    makeAutoObservable(this)
    this.updateFromJson(json)
  }

  updateFromJson(json: ITrack): void {
    for (const key in json) {
      this[key] = json[key]
    }
  }
}

export default Track
