import {makeAutoObservable, reaction, runInAction} from 'mobx'
import {IUserAuth, IUiStore, IPlayerState} from './types'
import RootStore from '../RootStore'
import {persistStoreKey} from '../utils'

class UiStore implements IUiStore {
  rootStore: RootStore
  loaders = 0
  userAuth = {} as IUserAuth
  playerState = {} as IPlayerState

  constructor(rootStore: RootStore) {
    makeAutoObservable(this)
    this.rootStore = rootStore
    persistStoreKey(() => this.userAuth, 'MOBX_ui_userAuth')
  }

  // Authentication
  setUserAuth(userAuth: IUserAuth): void {
    runInAction(() => {
      this.userAuth = userAuth
    })
  }

  clearUserAuth(): void {
    runInAction(() => {
      this.userAuth = {} as IUserAuth
    })
  }

  // Loading behaviour
  loadersInc(): void {
    this.loaders++
  }

  loadersDec(): void {
    this.loaders--
  }

  // Player
  setPlayerState(playerState: IPlayerState): void {
    this.playerState = {
      ...this.playerState,
      ...playerState
    }
  }
}

export default UiStore
