export interface IUserAuth {
  token_type: string
  access_token: string
  expires_in: string
}

export interface IUiStore {
  userAuth: IUserAuth | Record<string, unknown>
  loaders: number
}

export enum EnumPlayerStateCtxType {
  playlist = 'playlist',
  album = 'album',
  tracks = 'tracks'
}
export interface IPlayerState {
  contextType?: EnumPlayerStateCtxType
  // if contextType is 'playlist'
  playlistId?: string
  // if contextType is 'album'
  albumId?: string
  // if either contextType is 'playlist' | 'album'
  trackId?: string
  // if contextType is 'tracks'
  trackIds?: string[]
  // misc fields
  isPlaying?: boolean
  deviceId?: string
}
