import RootStore from '../RootStore'
import Album from './Album'
import {IAlbum} from './types'

class AlbumStore {
  rootStore: RootStore
  albums: Record<string, Album> = {}

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore
  }

  updateAlbumFromServer(json: IAlbum): void {
    const maybeAlbum = this.albums[json.id]
    if (maybeAlbum) {
      maybeAlbum.updateFromJson(json)
    } else {
      const album = new Album(json, this.rootStore)
      this.albums[json.id] = album
    }
  }
}

export default AlbumStore
