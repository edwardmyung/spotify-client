import {IArtist} from '../ArtistStore/types'
import {IImage} from '../types'

export interface IAlbum {
  album_type: string
  artists: IArtist[]
  available_markets: string[]
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  images: IImage[]
  name: string
  release_date: string
  release_date_precision: string
  total_tracks: number
  type: 'album'
  uri: string
}
