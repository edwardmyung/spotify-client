import {makeAutoObservable} from 'mobx'
import RootStore from '../RootStore'
import {IAlbum} from './types'
import {IArtist} from '../ArtistStore/types'

class Album implements IAlbum {
  rootStore: RootStore
  album_type
  artists
  available_markets
  external_urls
  href
  id
  images
  name
  release_date
  release_date_precision
  total_tracks
  type
  uri

  constructor(json: IAlbum, rootStore: RootStore) {
    makeAutoObservable(this)
    this.rootStore = rootStore
    this.updateFromJson(json)
  }

  updateFromJson(json: IAlbum): void {
    for (const key in json) {
      if (key === 'artists') {
        const artists: IArtist[] = json[key]
        artists.map(artist => this.rootStore.artist.updateArtistFromServer(artist))
      } else {
        this[key] = json[key]
      }
    }
  }
}

export default Album
