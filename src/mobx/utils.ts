import {reaction, toJS} from 'mobx'

export const persistStoreKey = (getValue: () => any, localStorageKey: string): void => {
  reaction(
    () => getValue(),
    val => {
      localStorage.setItem(localStorageKey, JSON.stringify(toJS(val)))
    }
  )
}

// Use reaction
// https://mobx.js.org/reactions.html

// reaction args:
// (1) data function: listen to value of the key
// (2) effect function: stringify that value and persist in localStorage

// call in constructor of each class, each-time for properties

// persistKey(this.userAuth)
