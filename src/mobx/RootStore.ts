import {makeAutoObservable} from 'mobx'
import PlaylistStore from './PlaylistStore'
import UiStore from './UiStore'
import TrackStore from './TrackStore'
import ArtistStore from './ArtistStore'
import AlbumStore from './AlbumStore'
import {IRootStore} from './types'

class RootStore implements IRootStore {
  playlist: PlaylistStore
  ui: UiStore
  track: TrackStore
  artist: ArtistStore
  album: AlbumStore

  constructor() {
    makeAutoObservable(this)

    this.playlist = new PlaylistStore(this)
    this.ui = new UiStore(this)
    this.track = new TrackStore(this)
    this.artist = new ArtistStore(this)
    this.album = new AlbumStore(this)

    Object.keys(localStorage).forEach(key => {
      if (!key.startsWith('MOBX_')) {
        return
      }
      const stringComponents = key.split('_')
      const storeKey = stringComponents[1]
      const propKey = stringComponents[2]
      this[storeKey][propKey] = JSON.parse(localStorage[key])
    })
  }
}

export default RootStore
