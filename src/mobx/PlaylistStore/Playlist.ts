import {makeAutoObservable} from 'mobx'
import {IPlaylist} from './types'

class Playlist implements IPlaylist {
  type = 'playlist'
  id
  collaborative
  description
  external_urls
  href
  images
  name
  owner
  primary_color
  public
  tracks
  uri

  constructor(json: IPlaylist) {
    makeAutoObservable(this)
    this.updateFromJson(json)
  }

  updateFromJson(json: IPlaylist): void {
    for (const key in json) {
      this[key] = json[key]
    }
  }
}

export default Playlist
