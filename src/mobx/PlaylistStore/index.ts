import {makeAutoObservable, runInAction} from 'mobx'
import * as api from '../../api'
import {IPlaylist, IPlaylistStore, IPlaylistWrappedTrack} from './types'
import Playlist from './Playlist'
import RootStore from '../RootStore'
import _ from 'lodash'

class PlaylistStore implements IPlaylistStore {
  rootStore: RootStore
  playlists = {} as Record<string, Playlist>
  userPlaylistIds = []
  userPlaylistOffset = 0

  constructor(rootStore: RootStore) {
    makeAutoObservable(this)
    this.rootStore = rootStore
  }

  updatePlaylistFromServer(json: IPlaylist): void {
    const maybePlaylist = this.playlists[json.id]
    const isPlaylistComplete = Array.isArray(json.tracks.items)

    if (isPlaylistComplete) {
      json.tracks.items = (json.tracks.items as IPlaylistWrappedTrack[]).map(wrappedTrack => {
        return this.rootStore.track.updateTrackFromServer(wrappedTrack.track)
      })
    }

    if (maybePlaylist) {
      // merge, picking the latest from json
      maybePlaylist.updateFromJson(_.merge(maybePlaylist, json))
    } else {
      const playlist = new Playlist(json)
      this.playlists[playlist.id] = playlist
    }
  }

  async fetchUserPlaylists(): Promise<void> {
    this.rootStore.ui.loadersInc()
    const LIMIT = 50
    try {
      const res = await fetch(
        api.playlist.fetchAuthedUserPlaylists(this.rootStore.ui.userAuth.access_token, LIMIT, this.userPlaylistOffset)
      )
      const {items} = await res.json()
      items.forEach(playlist => {
        this.updatePlaylistFromServer(playlist)
      })
      runInAction(() => {
        this.userPlaylistIds = [...this.userPlaylistIds, ...items.map(playlist => playlist.id)]
      })
      this.userPlaylistOffset += LIMIT
      this.rootStore.ui.loadersDec()
    } catch (err) {
      console.error(err)
      this.rootStore.ui.loadersDec()
    }
  }

  async fetchPlaylist(playlistId: string): Promise<void> {
    this.rootStore.ui.loadersInc()
    try {
      const res = await fetch(api.playlist.fetchPlaylist(this.rootStore.ui.userAuth.access_token, playlistId))
      const json = await res.json()
      this.updatePlaylistFromServer(json)
      this.rootStore.ui.loadersDec()
    } catch (err) {
      console.error(err)
      this.rootStore.ui.loadersDec()
    }
  }
}

export default PlaylistStore
