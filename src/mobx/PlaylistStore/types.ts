import RootStore from '../RootStore'
import Playlist from '../PlaylistStore/Playlist'
import {ITrack} from '../TrackStore/types'
import {IUser} from '../UserStore/types'
import {IImage} from '../types'

interface IPlaylistCore {
  id: string
  name: string
  collaborative: boolean
  description: string
  external_urls: {
    spotify: string
  }
  href: string
  images: IImage[]
  owner: IUser
  primary_color?: string
  tracks: {href: string; total: number}
  type: string
  uri: string
  public: boolean
}

export type IPlaylist = IPlaylistCore & {
  followers?: {
    href?: string
    total: number
  }
  tracks: {
    href: string
    total: number
    items?: ITrack[] | IPlaylistWrappedTrack[]
    limit?: number
    next?: string
    offset?: 0
    previous?: string
  }
}

export interface IPlaylistStore {
  rootStore: RootStore
  playlists: Record<string, Playlist>
  userPlaylistIds: string[]
}

export interface IPlaylistWrappedTrack {
  id: string
  name: string
  added_at: string
  added_by: IUser
  is_local: boolean
  primary_color?: string
  track: ITrack
  video_thumbnail: {
    url: null
  }
}
