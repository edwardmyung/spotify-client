import AlbumStore from './AlbumStore'
import ArtistStore from './ArtistStore'
import PlaylistStore from './PlaylistStore'
import TrackStore from './TrackStore'
import UiStore from './UiStore'

export interface IRootStore {
  playlist: PlaylistStore
  ui: UiStore
  track: TrackStore
  album: AlbumStore
  artist: ArtistStore
}

export interface IImage {
  height: number
  url: string
  width: number
}
