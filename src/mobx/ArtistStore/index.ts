import {makeAutoObservable} from 'mobx'
import RootStore from '../RootStore'
import Artist from './Artist'
import {IArtist} from './types'

class ArtistStore {
  rootStore: RootStore
  artists: Record<string, Artist> = {}

  constructor(rootStore: RootStore) {
    makeAutoObservable(this)
    this.rootStore = rootStore
  }

  updateArtistFromServer(json: IArtist): Artist {
    const artist = this.artists[json.id]
    if (!artist) {
      const artist = new Artist(json)
      this.artists[json.id] = artist
    } else {
      artist.updateFromJson(json)
    }

    return this.artists[json.id]
  }
}

export default ArtistStore
