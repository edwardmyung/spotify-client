import {makeAutoObservable} from 'mobx'
import {IArtist} from './types'

class Artist {
  constructor(json: IArtist) {
    makeAutoObservable(this)
    this.updateFromJson(json)
  }

  updateFromJson(json: IArtist): void {
    for (const key in json) {
      this[key] = json[key]
    }
  }
}

export default Artist
