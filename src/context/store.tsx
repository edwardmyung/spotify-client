import React from 'react'
import RootStore from '../mobx/RootStore'
import {IRootStore} from '../mobx/types'

const storeContext = React.createContext(null)

export const StoreProvider: React.FC = ({children}) => {
  const store = new RootStore()
  return <storeContext.Provider value={store}>{children}</storeContext.Provider>
}

export const useStore = (): IRootStore => {
  const store = React.useContext(storeContext)
  if (!store) {
    throw new Error('useStore must be used within a StoreProvider')
  }

  return store
}
