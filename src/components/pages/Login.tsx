import React from 'react'
import LoginBody from '../organisms/Login/Body'

const Login: React.FC = () => {
  return (
    <div style={{height: '100vh', position: 'relative'}}>
      <LoginBody />
    </div>
  )
}

export default Login
