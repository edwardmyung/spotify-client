import React from 'react'
import styled from 'styled-components'

export const TWrapper = styled.div`
  background: green;
  width: 500px;
`

const TBase = styled.div`
  width: 100%;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`

export const T1 = styled(TBase)`
  ${p =>
    p.theme.constants.typography.css.canon(p.theme.constants.typography.fontWeights.bold)}/* border:solid 1px red; */
`

export const T2 = styled(TBase)`
  ${p => p.theme.constants.typography.css.trafalgar(400)} // trafalgar
  border:solid 1px red;
  overflow: visible;
`

export const T3 = styled(TBase)`
  ${p => p.theme.constants.typography.css.paragon(400)} // paragon
  border:solid 1px red;
`

export const T4 = styled(TBase)`
  ${p => p.theme.constants.typography.css.doublePica(400)} // paragon
  border:solid 1px red;
`

export const T5 = styled(TBase)`
  ${p => p.theme.constants.typography.css.greatPrimer(400)} // paragon
  border:solid 1px red;
`

export const T6 = styled(TBase)`
  ${p => p.theme.constants.typography.css.bodyCopy(400)} // paragon
  border:solid 1px red;
`

export const T7 = styled(TBase)`
  ${p => p.theme.constants.typography.css.pica(700)} // paragon
  border:solid 1px red;
`

export const T8 = styled(TBase)`
  ${p => p.theme.constants.typography.css.longPrimer(700)} // paragon
  border:solid 1px red;
`

export const T9 = styled(TBase)`
  ${p => p.theme.constants.typography.css.brevier(400)} // paragon
  border:solid 1px red;
`

export const T10 = styled(TBase)`
  ${p => p.theme.constants.typography.css.minion(400)} // paragon
  border:solid 1px red;
`
const W = styled(TBase)`
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const HeaderContent = () => {
  return <>Header</>
}

export const Body = () => {
  return (
    <>
      <TWrapper>
        <W>
          <T1>
            Tier 1 heading
            <br />
            Tier 1 heading
          </T1>
          <T1></T1>
        </W>
        <T2>
          Tier 2 heading. Demo of multiple lines on this. There are multiple lines here, the margins at the top and
          bottom, and the line-height is preserved for the middle lines.
        </T2>
        <T3>Tier 3 heading</T3>
        <T4>Tier 4 heading</T4>
        <T5>Tier 5 heading</T5>
        <T6>Tier 6 heading</T6>
        <T7>Tier 7 heading</T7>
        <T8>Tier 8 heading</T8>
        <T9>Tier 9 heading</T9>
        <T10>Tier 10 heading</T10>
      </TWrapper>
    </>
  )
}
