import React from 'react'
import {useStore} from '../../../context/store'

export const HeaderContent = (): JSX.Element => {
  return <>Header</>
}

export const Body = (): JSX.Element => {
  const store = useStore()

  return (
    <div>
      <button onClick={() => store.ui.clearUserAuth()}>logout</button>
    </div>
  )
}
