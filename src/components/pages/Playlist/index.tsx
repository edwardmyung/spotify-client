import React, {useEffect, useState, useRef} from 'react'
import {useStore} from '../../../context/store'
import {observer} from 'mobx-react'
import PlaylistHeader from '../../organisms/Playlist/Header'
import PlaylistActionsRow from '../../organisms/Playlist/ActionsRow'
import PlaylistTable from '../../organisms/Playlist/Table'

import {GridMain} from '../../atoms/appLayout'
import AppLayoutHeader from '../../molecules/appLayout/Header'
import {HeaderContentOnScroll} from './headerContents'
import {colors} from '../../../style-guidelines/constants'
import LoadingSpinner from '../../atoms/LoadingSpinner'

export const Body = observer(
  ({match}): JSX.Element => {
    const playlistId = match.params.playlistId
    const store = useStore()
    const gridMainRef = useRef(null)

    useEffect(() => {
      store.playlist.fetchPlaylist(playlistId)
    }, [playlistId])

    useEffect(() => {
      const onScroll = e => setScrollTop(e.currentTarget.scrollTop)
      if (gridMainRef.current) {
        gridMainRef.current.addEventListener('scroll', onScroll)
      }

      return () => {
        if (gridMainRef.current) {
          gridMainRef.current.removeEventListener('scroll', onScroll)
        }
      }
    }, [gridMainRef.current])

    const playlist = store.playlist.playlists[playlistId]

    const [scrollTop, setScrollTop] = useState(0)
    const threshold = 200

    if (store.ui.loaders !== 0 || !playlist) {
      return <LoadingSpinner />
    }

    return (
      <GridMain scrollableNodeProps={{ref: gridMainRef}} style={{background: colors.grays[95]}}>
        <AppLayoutHeader
          defaultHeaderContent={null}
          onScrollHeaderContent={<HeaderContentOnScroll playlistName={playlist.name} />}
          defaultBackground='rgba(0,0,0,0)'
          onScrollBackground='#dd5e38'
          isScrollComplete={scrollTop > threshold}
        />
        <PlaylistHeader image={playlist.images[0]} playlistName={playlist.name} creator={playlist.owner} />
        <PlaylistActionsRow />
        <PlaylistTable playlist={playlist} />
      </GridMain>
    )
  }
)
