import styled from 'styled-components'

export const HeaderContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
`

export const PlaylistName = styled.div`
  text-overflow: ellipsis;
  overflow: hidden;
`
