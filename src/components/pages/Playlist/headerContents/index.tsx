import React from 'react'
import {HeaderContentWrapper, PlaylistName} from './styled'

export const HeaderContentOnScroll: React.FC<{playlistName: string}> = ({playlistName}): JSX.Element => {
  return (
    <HeaderContentWrapper>
      <PlaylistName>{playlistName}</PlaylistName>
    </HeaderContentWrapper>
  )
}
