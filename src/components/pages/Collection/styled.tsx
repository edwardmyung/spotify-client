import {NavLink} from 'react-router-dom'
import {TButtonSize, TButtonTheme} from '../../atoms/buttons/types'
import {pillButtonBaseStyle, pillButtonActiveStyle} from '../../atoms/buttons/PillButton/styled'
import styled from 'styled-components'

const ACTIVE_CLASS_NAME = 'nav-item-active'
export const PillButtonNavLink = styled(NavLink).attrs({activeClassName: ACTIVE_CLASS_NAME})`
  display: inline-block;
  ${pillButtonBaseStyle(TButtonSize.medium, TButtonTheme.primary)}
  &.${ACTIVE_CLASS_NAME} {
    ${pillButtonActiveStyle(TButtonTheme.primary)}
  }
`
