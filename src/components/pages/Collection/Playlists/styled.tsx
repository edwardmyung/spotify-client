import styled from 'styled-components'

export const GridContainer = styled.div`
  padding: 8px;
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-auto-rows: auto;
  gap: 8px 8px;
  width: 100%;
  background: purple;

  @media screen and (min-width: ${p => p.theme.constants.breakpoints.sm}px) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media screen and (min-width: ${p => p.theme.constants.breakpoints.md}px) {
    grid-template-columns: repeat(5, 1fr);
  }
  @media screen and (min-width: ${p => p.theme.constants.breakpoints.lg}px) {
    grid-template-columns: repeat(6, 1fr);
  }
  @media screen and (min-width: ${p => p.theme.constants.breakpoints.xl}px) {
    grid-template-columns: repeat(7, 1fr);
  }
`

export const Test = styled.div`
  background: red;
  overflow: hidden;
`

export const TestDouble = styled.div`
  background-color: green;

  @media screen and (min-width: ${p => p.theme.constants.breakpoints.sm}px) {
    grid-column: auto / span 2;
  }
`
