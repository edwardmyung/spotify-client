import React from 'react'
import {GridContainer, Test, TestDouble} from './styled'

const Ele = () => (
  <Test>
    <img src='https://assets.dryicons.com/uploads/icon/svg/8321/e64d3204-1622-4a66-91dc-467d5568c6fa.svg' />
    <div style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}}>
      Test artist test test test test test test test
    </div>
    <div>Test album name</div>
  </Test>
)

const Playlists = (): JSX.Element => {
  return (
    <GridContainer>
      <TestDouble>test</TestDouble>
      {Array.from({length: 15}).map((_, ix) => (
        <Ele key={ix} />
      ))}
    </GridContainer>
  )
}

export default Playlists
