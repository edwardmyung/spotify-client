import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {PillButtonAsNavLink} from '../../atoms/buttons/PillButton'
import {TButtonSize, TButtonTheme} from '../../atoms/buttons/types'

import Playlists from './Playlists'

const Podcasts = () => {
  return <div>Podcasts</div>
}

const Artists = () => {
  return <div>Artists</div>
}

const Albums = () => {
  return <div>Albums</div>
}

export const HeaderContent: React.FC<{match: Record<string, unknown>}> = ({match}) => {
  return (
    <>
      <PillButtonAsNavLink size={TButtonSize.medium} to={`${match.url}/playlists`} inline>
        Playlists
      </PillButtonAsNavLink>
      <PillButtonAsNavLink size={TButtonSize.medium} to={`${match.url}/podcasts`} inline>
        Podcasts
      </PillButtonAsNavLink>
      <PillButtonAsNavLink size={TButtonSize.medium} to={`${match.url}/artists`} inline>
        Artists
      </PillButtonAsNavLink>
      <PillButtonAsNavLink size={TButtonSize.medium} to={`${match.url}/albums`} inline>
        Albums
      </PillButtonAsNavLink>
    </>
  )
}

export const Body: React.FC<{match: any}> = ({match}) => {
  return (
    <>
      <Switch>
        <Route path={`${match.url}/playlists`} component={Playlists} />
        <Route path={`${match.url}/podcasts`} component={Podcasts} />
        <Route path={`${match.url}/artists`} component={Artists} />
        <Route path={`${match.url}/albums`} component={Albums} />
        <Redirect from={`${match.url}`} to={`${match.url}/playlists`} />
      </Switch>
    </>
  )
}

// const Collection: React.FC<{match: any}> = observer(({match}) => {
//   return <Body match={match} />
// })
