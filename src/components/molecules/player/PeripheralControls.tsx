import React, {useEffect, useState} from 'react'
import VolumeZero from '../../atoms/icons/VolumeZero'
import VolumeOne from '../../atoms/icons/VolumeOne'
import VolumeTwo from '../../atoms/icons/VolumeTwo'
import Slider from '../../atoms/Slider'
import {PeripheralControlsWrapper} from './styled'

const PeripheralControls: React.FC<{initialVolume: number; setPlayerVolume: (volume: number) => void}> = ({
  initialVolume = 0,
  setPlayerVolume
}) => {
  let volumeIconEle
  const [volume, setVolume] = useState(initialVolume)

  useEffect(() => {
    setVolume(initialVolume)
  }, [initialVolume])

  if (volume === 0) {
    volumeIconEle = <VolumeZero fill='white' size={25} />
  } else if (volume < 0.45) {
    volumeIconEle = <VolumeOne fill='white' size={25} />
  } else {
    volumeIconEle = <VolumeTwo fill='white' size={25} />
  }

  return (
    <PeripheralControlsWrapper>
      {volumeIconEle}
      <Slider
        width='100px'
        min={0}
        max={1}
        step={0.1}
        style={{marginRight: 10, marginLeft: 10}}
        value={volume}
        onChange={(e: any) => {
          setVolume(parseFloat(e.target.value))
          setPlayerVolume(parseFloat(e.target.value))
        }}
      />
    </PeripheralControlsWrapper>
  )
}

export default PeripheralControls
