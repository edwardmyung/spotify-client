import React from 'react'
import {IArtist} from '../../../mobx/ArtistStore/types'
import {ITrack} from '../../../mobx/TrackStore/types'
import {
  TrackInfoInset,
  TrackInfoImageInset,
  TrackInfoWrapper,
  TrackInfoPlaceholderWrapper,
  TrackInfoName,
  TrackInfoArtistsName
} from './styled'
import PlaceholderRectangle from '../../atoms/placeholder-rectangle'

const IMAGE_SIZE = '60px'

const TrackInfo: React.FC<{track: ITrack; artists: IArtist[]}> = ({track, artists = []}) => {
  if (artists.length === 0) {
    return (
      <TrackInfoPlaceholderWrapper>
        <TrackInfoImageInset>
          <PlaceholderRectangle width={IMAGE_SIZE} height={IMAGE_SIZE} />
        </TrackInfoImageInset>
        <TrackInfoInset>
          <PlaceholderRectangle width='100%' height='16px' style={{marginBottom: 6}} />
          <PlaceholderRectangle width='80%' height='14px' />
        </TrackInfoInset>
      </TrackInfoPlaceholderWrapper>
    )
  }

  return (
    <TrackInfoWrapper>
      <TrackInfoImageInset>
        <img
          src={track.album.images[track.album.images.length - 1].url}
          style={{width: IMAGE_SIZE, height: IMAGE_SIZE}}
        />
      </TrackInfoImageInset>
      <TrackInfoInset>
        <TrackInfoName>{track.name}</TrackInfoName>
        <TrackInfoArtistsName>
          {artists.map((artist: IArtist, ix: number) => {
            const maybeComma = ix !== 0 && ', '
            return (
              <span key={artist.uri}>
                {maybeComma}
                {artist.name}
              </span>
            )
          })}
        </TrackInfoArtistsName>
      </TrackInfoInset>
    </TrackInfoWrapper>
  )
}

export default TrackInfo
