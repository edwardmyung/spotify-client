import React from 'react'
import ControlPrev from '../../atoms/icons/ControlPrev'
import ControlPlay from '../../atoms/icons/ControlPlay'
import ControlPause from '../../atoms/icons/ControlPause'
import ControlNext from '../../atoms/icons/ControlNext'
import IconButton, {EnumIconButtonFill} from '../../atoms/buttons/IconButton'
import {
  CentralControlsWrapper,
  CentralControlsButtonsWrapper,
  DurationWrapper,
  DurationTime,
  DurationTimeline,
  DurationTimelineProgress
} from './styled'

interface ICentralControlsProps {
  isPlaying: boolean
  togglePlay: () => void
  prevTrack: () => void
  nextTrack: () => void
}

const DurationMeter: React.FC<{positionMs: number; durationMs}> = ({positionMs, durationMs}) => {
  return (
    <DurationWrapper>
      <DurationTime>1:15</DurationTime>
      <DurationTimeline>
        <DurationTimelineProgress pctCompletion={(positionMs / durationMs) * 100} />
      </DurationTimeline>
      <DurationTime>3:30</DurationTime>
    </DurationWrapper>
  )
}

const PlayPauseButton = ({isPlaying, togglePlay}) => {
  const onClick = () => {
    togglePlay()
  }
  return (
    <IconButton onClick={onClick} fill={EnumIconButtonFill.default} size={35} circle>
      {isPlaying ? (
        <ControlPause size={25} fill={EnumIconButtonFill.delegate} />
      ) : (
        <ControlPlay size={25} fill={EnumIconButtonFill.delegate} />
      )}
    </IconButton>
  )
}

const CentralControls: React.FC<ICentralControlsProps> = ({isPlaying, togglePlay, prevTrack, nextTrack}) => {
  const position = 50
  const duration = 100
  return (
    <CentralControlsWrapper>
      <CentralControlsButtonsWrapper>
        <IconButton onClick={() => prevTrack()} fill={EnumIconButtonFill.default} size={35}>
          <ControlPrev fill='delegate' size={25} />
        </IconButton>

        <PlayPauseButton isPlaying={isPlaying} togglePlay={togglePlay} />

        <IconButton onClick={() => nextTrack()} fill={EnumIconButtonFill.default} size={35}>
          <ControlNext fill='delegate' size={25} />
        </IconButton>
      </CentralControlsButtonsWrapper>

      <DurationMeter positionMs={position} durationMs={duration} />
    </CentralControlsWrapper>
  )
}

export default CentralControls
