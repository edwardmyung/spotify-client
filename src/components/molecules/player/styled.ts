import styled from 'styled-components'
import {FontWeight} from '../../../style-guidelines/constants/typography'

/*
  Track info
*/
export const TrackInfoInset = styled.div`
  ${p => p.theme.constants.space.css.inset.s}
  flex-grow: 1;
  color: ${p => p.theme.constants.colors.white};
  overflow: hidden;
`

export const TrackInfoImageInset = styled.div`
  ${p => p.theme.constants.space.css.inset.s}
`

export const TrackInfoWrapper = styled.div`
  ${p => p.theme.constants.space.css.inset.s}
  display: flex;
  height: 100%;
  width: 100%;
  align-items: center;
`

export const TrackInfoPlaceholderWrapper = styled.div`
  ${p => p.theme.constants.space.css.inset.s}
  color: white;
  display: flex;
  align-items: center;
  height: 100%;
`

export const TrackInfoName = styled.div`
  ${p => p.theme.constants.space.css.inset.xs}
  ${p => p.theme.constants.typography.css.brevier(FontWeight.regular)}
  
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 100%;
`

export const TrackInfoArtistsName = styled.div`
  ${p => p.theme.constants.space.css.inset.xs}
  ${p => p.theme.constants.typography.css.brevier(FontWeight.regular)}
  color: rgba(255,255,255,0.5);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 100%;
`

/*
  Peripheral controls
*/
export const PeripheralControlsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  height: 100%;
`

/* 
  Central Controls
*/

export const CentralControlsWrapper = styled.div`
  width: 100%;
  max-width: 400px;
`

export const CentralControlsButtonsWrapper = styled.div`
  ${p => p.theme.constants.space.css.stack.s}
  display: flex;
  justify-content: center;
`

export const DurationWrapper = styled.div`
  display: flex;
  align-items: center;
`

export const DurationTime = styled.div`
  color: white;
  ${p => p.theme.constants.typography.css.minion(FontWeight.regular)}
`

export const DurationTimeline = styled.div`
  ${p => p.theme.constants.space.css.stack.s}
  background: ${p => p.theme.constants.colors.grays[70]};
  height: 3px;
  flex-grow: 1;
  border-radius: 60px;
  overflow: hidden;
  margin: 0px 10px;
`

export const DurationTimelineProgress = styled.div<{
  pctCompletion: number
}>`
  width: ${p => p.pctCompletion}%;
  height: 100%;
  background: ${p => p.theme.constants.colors.grays[30]};
`
