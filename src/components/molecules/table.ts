import styled from 'styled-components'
import {FontWeight} from '../../style-guidelines/constants/typography'

export const GridTableRow = styled.div<{
  isSelectable?: boolean
  isSelected?: boolean
}>`
  display: grid;
  grid-template-columns: 40px 1fr 0.5fr 80px;
  min-height: 0;
  min-width: 0;
  user-select: none;
  pointer-events: default;
  border-radius: 4px;

  ${p =>
    p.isSelectable &&
    !p.isSelected &&
    `
    &:hover {
      background: ${p.theme.constants.colors.grays[80]};
    }
  `}
  ${p =>
    p.isSelectable &&
    p.isSelected &&
    `
    background: ${p.theme.constants.colors.grays[70]};
  }
  `}
`

export const GridTableCell = styled.span`
  ${p => p.theme.constants.space.css.inset.s}
  overflow: hidden;
  min-width: 0;
  display: flex;
  align-items: center;
`

/*
  A styled-component that ensures text overflow is clipped off, and width is completely governed by 
  css grid. Extend it and apply typography styles at the point of use.
*/
export const GridTableCellOverflowClip = styled.div`
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const GridTableHeaderCell = styled(GridTableCell)`
  text-transform: uppercase;
  letter-spacing: 0.1em;
  ${p => p.theme.constants.typography.css.minion(FontWeight.regular)}
  color: ${p => p.theme.constants.colors.grays[60]};
`

export const GridTableHr = styled.hr`
  border: none;
  height: 1px;
  background-color: ${p => p.theme.constants.colors.grays[80]};
`
