import React, {useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import {useTransition, useSpring} from 'react-spring'
import CircleButton from '../../../atoms/buttons/CircleButton'
import ChevronLeft from '../../../atoms/icons/ChevronLeft'
import ChevronRight from '../../../atoms/icons/ChevronRight'
import space from '../../../../style-guidelines/constants/space'
import {AppLayoutHeader} from '../../../atoms/appLayout'
import {CircleButtonsWrapper, ContentWrapper} from './styled'

interface IProps {
  defaultBackground?: 'rgba(0,0,0,0)' | string
  onScrollBackground?: null | string
  defaultHeaderContent?: JSX.Element
  onScrollHeaderContent?: JSX.Element
  isScrollComplete: boolean
}

const Header: React.FC<IProps> = ({
  defaultHeaderContent = null,
  onScrollHeaderContent = null,
  defaultBackground = 'rgba(0,0,0,0)',
  onScrollBackground = 'rgba(0,0,0,0)',
  isScrollComplete
}) => {
  const history = useHistory()

  const onClickBack = () => {
    history.goBack()
  }

  const onClickForward = () => {
    history.goForward()
  }

  const transitions = useTransition(isScrollComplete, null, {
    from: {opacity: 0},
    enter: {opacity: 1},
    leave: {opacity: 0}
  })

  const [wrapperProps, setWrapperProps] = useSpring(() => ({
    background: defaultBackground
  }))

  useEffect(() => {
    setWrapperProps({
      background: isScrollComplete ? onScrollBackground : defaultBackground
    })
  }, [isScrollComplete])

  return (
    <AppLayoutHeader style={wrapperProps}>
      <CircleButtonsWrapper>
        <CircleButton
          onClick={onClickBack}
          content={<ChevronLeft fill='white' size={28} />}
          disabled={false}
          addCss={space.css.inline.right.s}
        />
        <CircleButton onClick={onClickForward} content={<ChevronRight fill='white' size={28} />} disabled={false} />
      </CircleButtonsWrapper>
      <div
        style={{
          width: '100%',
          height: '100%',
          position: 'relative',
          flexGrow: 1
        }}>
        {transitions.map(({item, key, props}) =>
          item ? (
            <ContentWrapper key={key} style={props}>
              {onScrollHeaderContent}
            </ContentWrapper>
          ) : (
            <ContentWrapper key={key} style={props}>
              {defaultHeaderContent}
            </ContentWrapper>
          )
        )}
      </div>

      <div
        style={{
          color: 'white',
          padding: '5px 20px',
          background: 'black',
          borderRadius: 20,
          fontSize: 14,
          marginRight: 5
        }}>
        edward.myung
      </div>
    </AppLayoutHeader>
  )
}

export default Header
