import styled from 'styled-components'
import {animated} from 'react-spring'

export const CircleButtonsWrapper = styled.div`
  ${p => p.theme.constants.space.css.inset.s}
  display: flex;
  align-items: center;
`

export const ContentWrapper = styled(animated.div)`
  /* background: purple; */
  ${p => p.theme.constants.space.css.inset.s}
  position: absolute;
  color: white;
  width: 100%;
  height: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`
