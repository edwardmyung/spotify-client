import styled from 'styled-components'
import {FontWeight} from '../../../../style-guidelines/constants/typography'

export const LogoIconWrapper = styled.div`
  ${p => p.theme.constants.space.css.insetSquish.m}
`

export const CoreAppNavWrapper = styled.div`
  ${p => p.theme.constants.space.css.stack.m}
`

export const FlexWrapper = styled.div`
  ${p => p.theme.app.layout.sidebar.insetCss}
  background: ${p => p.theme.app.layout.sidebar.background};
  display: flex;
  flex-flow: column;
  height: 100%;
`

export const FlexChildStaticHead = styled.div`
  flex: 0 0 auto;
`

export const FlexChildScrollable = styled.div`
  flex: 1 1 0px;
  overflow: auto;
`

export const FlexChildStaticFooter = styled.div`
  ${p => p.theme.constants.space.css.insetSquish.m}
  ${p => p.theme.constants.typography.css.brevier(FontWeight.bold)}
  flex: 0 0 auto;
  color: white;
`

export const PlaylistHr = styled.hr`
  border-color: rgba(255, 255, 255, 0.1);
  height: 1px;
`

export const PlaylistActionIconWrapper = styled.div`
  width: 30px;
  height: 30px;
  background: red;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
`

export const PlaylistAction = styled.button`
  ${p => p.theme.constants.space.css.insetSquish.m}
  ${p => p.theme.constants.typography.css.brevier(FontWeight.bold)}
  padding-top: 5px;
  padding-bottom: 5px;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  background: none;
  border: none;
  color: white;
`

export const PlaylistsUl = styled.ul`
  ${p => p.theme.constants.space.css.insetSquish.m}
  padding-top:0px;
  padding-bottom: 0px;
  margin: 0px;
  list-style: none;
`

export const PlaylistsLi = styled.li``
