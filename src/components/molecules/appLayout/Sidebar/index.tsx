import React from 'react'
import {Link} from 'react-router-dom'
import {
  LogoIconWrapper,
  FlexWrapper,
  FlexChildStaticHead,
  FlexChildStaticFooter,
  FlexChildScrollable,
  CoreAppNavWrapper,
  PlaylistHr,
  PlaylistAction,
  PlaylistActionIconWrapper,
  PlaylistsUl,
  PlaylistsLi
} from './styled'
import LogoIcon from '../../../atoms/icons/Logo'
import HomeIcon from '../../../atoms/icons/Home'
import LibraryIcon from '../../../atoms/icons/Library'
import SearchIcon from '../../../atoms/icons/Search'
import PlusIcon from '../../../atoms/icons/Plus'
import HeartIcon from '../../../atoms/icons/Heart'
import {PillButtonAsNavLink} from '../../../atoms/buttons/PillButton'
import TextButton from '../../../atoms/buttons/TextButton'
import {TButtonSize} from '../../../atoms/buttons/types'
import {IPlaylist} from '../../../../mobx/PlaylistStore/types'

export interface AppLayoutSidebarProps {
  user?: Record<string, unknown>
  userPlaylists: IPlaylist[]
  fetchUserPlaylists: () => void
}

const CoreAppNav = () => (
  <CoreAppNavWrapper>
    <PillButtonAsNavLink exact size={TButtonSize.medium} to='/'>
      <HomeIcon fill='white' style={{marginRight: 10}} /> Home
    </PillButtonAsNavLink>
    <PillButtonAsNavLink size={TButtonSize.medium} to='/browse'>
      <SearchIcon fill='white' style={{marginRight: 10}} /> Search
    </PillButtonAsNavLink>
    <PillButtonAsNavLink size={TButtonSize.medium} to='/collection'>
      <LibraryIcon fill='white' style={{marginRight: 10}} /> Your Library
    </PillButtonAsNavLink>
  </CoreAppNavWrapper>
)

const AppLayoutSidebar: React.FC<AppLayoutSidebarProps> = ({user = null, userPlaylists = [], fetchUserPlaylists}) => {
  return (
    <FlexWrapper>
      <FlexChildStaticHead>
        <LogoIconWrapper>
          <LogoIcon fill='white' width={120} />
        </LogoIconWrapper>

        <CoreAppNav />

        <div style={{padding: '12px 16px', color: 'white', fontSize: 12, margin: 0}}>PLAYLISTS</div>

        <PlaylistAction>
          <PlaylistActionIconWrapper>
            <PlusIcon fill='white' />
          </PlaylistActionIconWrapper>
          Create Playlist
        </PlaylistAction>
        <PlaylistAction>
          <PlaylistActionIconWrapper>
            <HeartIcon fill='white' />
          </PlaylistActionIconWrapper>
          Liked Songs
        </PlaylistAction>
        <PlaylistHr />
      </FlexChildStaticHead>
      <FlexChildScrollable>
        <PlaylistsUl>
          {userPlaylists.map((pl, ix) => (
            <PlaylistsLi key={ix}>
              <Link to={`/playlist/${pl.id}`}>
                <TextButton
                  size={TButtonSize.small}
                  style={{
                    paddingLeft: 0,
                    paddingRight: 0,
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',
                    overflow: 'hidden'
                  }}>
                  {pl.name}
                </TextButton>
              </Link>
            </PlaylistsLi>
          ))}
        </PlaylistsUl>

        <button onClick={() => fetchUserPlaylists()}>Load more</button>
      </FlexChildScrollable>
      <FlexChildStaticFooter>Install App</FlexChildStaticFooter>
    </FlexWrapper>
  )
}

export default AppLayoutSidebar
