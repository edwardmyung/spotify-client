import React from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import {Story, Meta} from '@storybook/react/types-6-0'

import AppLayoutSidebar, {AppLayoutSidebarProps} from './index'
import StoryRouter from 'storybook-react-router'
// import '../../../index.css'

export default {
  title: 'Organisms/AppLayoutSidebar',
  component: AppLayoutSidebar,
  decorators: [
    StoryRouter(),
    Story => (
      <div style={{width: 240, height: '100vh'}}>
        <Story />
      </div>
    )
  ]
} as Meta

const Template: Story<AppLayoutSidebarProps> = args => <AppLayoutSidebar {...args} />

export const LoggedIn = Template.bind({})
LoggedIn.args = {
  user: {}
}

export const LoggedOut = Template.bind({})
LoggedOut.args = {}
