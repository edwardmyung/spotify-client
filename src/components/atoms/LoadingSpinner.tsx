import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  background: ${p => p.theme.constants.colors.grays[90]};
  display: flex;
  align-items: center;
  justify-content: center;
`

const LoadingSpinner: React.FC = () => {
  return (
    <Wrapper>
      <svg xmlns='http://www.w3.org/2000/svg' width='60' fill='#fff' viewBox='0 0 120 30'>
        <circle cx='15' cy='15' r='15'>
          <animate
            attributeName='r'
            begin='0s'
            calcMode='linear'
            dur='0.8s'
            from='15'
            repeatCount='indefinite'
            to='15'
            values='15;9;15'></animate>
          <animate
            attributeName='fill-opacity'
            begin='0s'
            calcMode='linear'
            dur='0.8s'
            from='1'
            repeatCount='indefinite'
            to='1'
            values='1;.5;1'></animate>
        </circle>
        <circle cx='60' cy='15' r='9' fillOpacity='0.3'>
          <animate
            attributeName='r'
            begin='0s'
            calcMode='linear'
            dur='0.8s'
            from='9'
            repeatCount='indefinite'
            to='9'
            values='9;15;9'></animate>
          <animate
            attributeName='fill-opacity'
            begin='0s'
            calcMode='linear'
            dur='0.8s'
            from='0.5'
            repeatCount='indefinite'
            to='0.5'
            values='.5;1;.5'></animate>
        </circle>
        <circle cx='105' cy='15' r='15'>
          <animate
            attributeName='r'
            begin='0s'
            calcMode='linear'
            dur='0.8s'
            from='15'
            repeatCount='indefinite'
            to='15'
            values='15;9;15'></animate>
          <animate
            attributeName='fill-opacity'
            begin='0s'
            calcMode='linear'
            dur='0.8s'
            from='1'
            repeatCount='indefinite'
            to='1'
            values='1;.5;1'></animate>
        </circle>
      </svg>
    </Wrapper>
  )
}

export default LoadingSpinner
