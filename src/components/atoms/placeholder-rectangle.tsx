import React from 'react'
import styled from 'styled-components'

const Rect = styled.div<{width: string; height: string}>`
  width: ${p => p.width};
  height: ${p => p.height};
  border-radius: 5px;
  background: rgba(255, 255, 255, 0.1);
`

const PlaceholderRectangle: React.FC<{
  width: string
  height: string
  style?: React.CSSProperties
}> = ({width, height, style = {}}) => {
  return <Rect width={width} height={height} style={style} />
}

export default PlaceholderRectangle
