import styled from 'styled-components'
import {animated} from 'react-spring'

export const GridContainer = styled.div`
  display: grid;
  height: 100vh;
  grid-template-columns: 240px auto;
  grid-template-rows: auto 85px;
  grid-template-areas:
    'aside main'
    'footer footer';
`
export const GridAside = styled.aside`
  grid-area: aside;
`

import SimpleBar from 'simplebar-react'
export const GridMain = styled(SimpleBar)`
  grid-area: main;
  overflow: auto;
  max-height: 100%;
`

export const GridFooter = styled.footer`
  grid-area: footer;
  background: ${p => p.theme.app.layout.footer.background};
`

const HEADER_HEIGHT = 60
export const AppLayoutHeader = styled(animated.div)`
  position: sticky;
  top: 0px;
  height: ${HEADER_HEIGHT}px;
  display: flex;
  align-items: center;
`
