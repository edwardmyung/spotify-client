import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M12.5 38.5L30.2083 26L12.5 13.5V38.5ZM33.3333 13.5V38.5H37.5V13.5H33.3333Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
