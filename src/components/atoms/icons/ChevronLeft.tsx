import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M31.7782 13L19 25.7782L20.4142 27.1924L33.1924 14.4142L31.7782 13Z' fill={fill} />
      <path d='M19 25.7782L31.7782 38.5563L33.1924 37.1421L20.4142 24.3639L19 25.7782Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
