import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M37.5 38.5L19.7917 26L37.5 13.5V38.5ZM16.6667 13.5V38.5H12.5V13.5H16.6667Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
