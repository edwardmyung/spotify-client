import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M27.2857 10H22.7143V23.7143H9V28.2857H22.7143V42H27.2857V28.2857H41V23.7143H27.2857V10Z'
        fill={fill}
      />
    </BaseSvgIcon>
  )
}

export default Icon
