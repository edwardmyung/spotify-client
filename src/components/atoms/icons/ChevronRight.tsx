import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M20.4142 13L33.1924 25.7782L31.7782 27.1924L19 14.4142L20.4142 13Z' fill={fill} />
      <path d='M33.1924 25.7782L20.4142 38.5563L19 37.1421L31.7782 24.3639L33.1924 25.7782Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
