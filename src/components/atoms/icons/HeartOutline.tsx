import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, stroke = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style} fill='none'>
      <path
        d='M23.5931 38.2741L23.5909 38.2722C19.2691 34.3531 15.8037 31.207 13.4006 28.2691C11.0148 25.3522 9.83331 22.8239 9.83331 20.1667C9.83331 15.8618 13.1951 12.5 17.5 12.5C19.9458 12.5 22.3167 13.6459 23.858 15.4558L25 16.7969L26.142 15.4558C27.6833 13.6459 30.0542 12.5 32.5 12.5C36.8049 12.5 40.1666 15.8618 40.1666 20.1667C40.1666 22.824 38.9851 25.3523 36.599 28.2715C34.1959 31.2115 30.7309 34.3614 26.4095 38.2884C26.4089 38.2889 26.4084 38.2894 26.4079 38.2899L25.0038 39.5584L23.5931 38.2741Z'
        stroke={stroke}
        strokeWidth='3'
      />
    </BaseSvgIcon>
  )
}

export default Icon
