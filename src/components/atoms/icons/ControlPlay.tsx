import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M16 42L38.6667 26L16 10V42Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
