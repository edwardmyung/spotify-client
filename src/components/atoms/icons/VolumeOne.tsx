import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M7 31.4863V19.5137H15L25 9.5365V41.4635L15 31.4863H7ZM29 17.4584C31.96 18.935 34 21.9681 34 25.5C34 29.0319 31.96 32.065 29 33.5217V30.9119C30.8399 29.5895 32 27.6636 32 25.5081C32 23.3525 30.8399 21.4266 29 20.0973V17.4584Z'
        fill={fill}
      />
    </BaseSvgIcon>
  )
}

export default Icon
