import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M7 19.5137V31.4863H15L25 41.4635V9.5365L15 19.5137H7Z' fill={fill} />
      <path
        d='M40 22.1079L38.8921 21L34.5 25.3921L30.1079 21L29 22.1079L33.3921 26.5L29 30.8921L30.1079 32L34.5 27.6079L38.8921 32L40 30.8921L35.6079 26.5L40 22.1079Z'
        fill={fill}
      />
    </BaseSvgIcon>
  )
}

export default Icon
