import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M21.2 41V29.3529H28.8V41H38.3V25.4706H44L25 8L6 25.4706H11.7V41H21.2Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
