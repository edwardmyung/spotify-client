import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <rect x='8' y='9' width='3.77718' height='32.106' fill={fill} />
      <rect x='17.443' y='9' width='3.77718' height='32.106' fill={fill} />
      <rect
        x='26.8859'
        y='10.6601'
        width='3.77718'
        height='34.245'
        transform='rotate(-26.0722 26.8859 10.6601)'
        fill={fill}
      />
    </BaseSvgIcon>
  )
}

export default Icon
