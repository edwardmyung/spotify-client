export interface IBaseIcon {
  size: number
  style: React.CSSProperties
  children: React.ReactNode
  fill?: string
}

export interface IIcon {
  fill?: string
  stroke?: string
  size?: number
  style?: React.CSSProperties
}
