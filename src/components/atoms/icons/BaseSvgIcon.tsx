import React from 'react'
import {IBaseIcon} from './types'

export const BaseSvgIcon: React.FC<IBaseIcon> = ({size, style, children, fill}) => {
  return (
    <svg
      width={size}
      height={size}
      fill={fill} // 'none' is set here for some outline-only svgs
      viewBox='0 0 50 50'
      xmlns='http://www.w3.org/2000/svg'
      style={{flexShrink: 0, ...style}}>
      {children}
    </svg>
  )
}
