import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path
        d='M7 19.5137V31.4863H15L25 41.4635V9.53649L15 19.5137H7ZM34 25.5C34 21.9681 31.96 18.935 29 17.4584V33.5217C31.96 32.065 34 29.0319 34 25.5ZM29 8V12.1106C34.78 13.8267 39 19.1745 39 25.5C39 31.8255 34.78 37.1733 29 38.8894V43C37.02 41.1842 43 34.0405 43 25.5C43 16.9595 37.02 9.81585 29 8V8Z'
        fill={fill}
      />
    </BaseSvgIcon>
  )
}

export default Icon
