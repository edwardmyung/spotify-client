import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path
        d='M25 41.5833L22.5833 39.3833C14 31.6 8.33331 26.4667 8.33331 20.1667C8.33331 15.0333 12.3666 11 17.5 11C20.4 11 23.1833 12.35 25 14.4833C26.8166 12.35 29.6 11 32.5 11C37.6333 11 41.6666 15.0333 41.6666 20.1667C41.6666 26.4667 36 31.6 27.4166 39.4L25 41.5833Z'
        fill={fill}
      />
    </BaseSvgIcon>
  )
}

export default Icon
