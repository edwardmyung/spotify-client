import React from 'react'
import {BaseSvgIcon} from './BaseSvgIcon'
import {IIcon} from './types'

const Icon: React.FC<IIcon> = ({size = 18, fill = 'black', style = {}}) => {
  return (
    <BaseSvgIcon size={size} style={style}>
      <path d='M16 38V13H22V38H16Z' fill={fill} />
      <path d='M28 38V13H34V38H28Z' fill={fill} />
    </BaseSvgIcon>
  )
}

export default Icon
