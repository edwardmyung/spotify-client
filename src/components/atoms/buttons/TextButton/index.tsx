import React from 'react'
import {ButtonWrapper} from './styled'
import {TButtonSize} from '../types'

const TextButton: React.FC<{size: TButtonSize; style: React.CSSProperties}> = ({children, size, style, ...props}) => {
  return (
    <ButtonWrapper size={size} style={style} {...props}>
      {children}
    </ButtonWrapper>
  )
}

export default TextButton
