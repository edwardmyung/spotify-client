import styled from 'styled-components'
import {TButtonSize, TButtonTheme} from '../types'

export const ButtonWrapper = styled.button<{size: TButtonSize}>`
  ${p => p.theme.constants.space.css.insetSquish.s}
  background: none;
  width: 100%;
  border: none;
  text-align: left;
  cursor: pointer;
  color: ${p => p.theme.app.buttons.textButton.colors[TButtonTheme.primary].inactiveColor};
  &:hover {
    color: ${p => p.theme.app.buttons.textButton.colors[TButtonTheme.primary].activeColor};
  }
`
