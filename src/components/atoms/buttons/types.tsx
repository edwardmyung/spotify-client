export enum TButtonSize {
  small = 'small',
  medium = 'medium',
  large = 'large'
}

export enum TButtonTheme {
  primary = 'primary',
  secondary = 'secondary',
  brand = 'brand'
}
