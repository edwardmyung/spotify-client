import styled from 'styled-components'
import {TButtonTheme} from '../types'

export const ButtonWrapper = styled.button<{addCss?: string; buttonTheme: TButtonTheme; size?: number}>`
  background: ${p => {
    if (p.disabled) {
      return p.theme.app.buttons.circleButton.colors[p.buttonTheme].disabledBackground
    }

    return p.theme.app.buttons.circleButton.colors[p.buttonTheme].background
  }};
  cursor: ${p => {
    if (p.disabled) return 'not-allowed'
    return 'pointer'
  }};
  border: none;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  border-radius: ${p => p.size}px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  ${({addCss}) => addCss}
  outline: none;

  &:focus {
    ${p => p.theme.app.accessibilityOutline};
  }
`
