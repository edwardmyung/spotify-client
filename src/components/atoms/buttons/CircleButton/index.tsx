import React from 'react'
import {TButtonTheme} from '../types'
import {ButtonWrapper} from './styled'

interface ICircleButtonProps {
  onClick: () => void
  content: JSX.Element
  style?: React.CSSProperties
  addCss?: string // Additional full css for space-token customisations
  disabled?: boolean
  buttonTheme?: TButtonTheme
  size?: number
}

const CircleButton: React.FC<ICircleButtonProps> = ({
  content,
  buttonTheme = TButtonTheme.primary,
  size = 32,
  ...props
}) => {
  return (
    <ButtonWrapper {...props} buttonTheme={buttonTheme} size={size}>
      {content}
    </ButtonWrapper>
  )
}

export default CircleButton
