import React from 'react'
import styled from 'styled-components'

export enum EnumIconButtonFill {
  default = 'default',
  delegate = 'delegate'
}

const IconButtonBaseWrapper = styled.button<{
  size: number
  fill?: EnumIconButtonFill
  stroke?: EnumIconButtonFill
}>`
  cursor: pointer;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  flex-shrink: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  background: none;
  transition: fill 0.3s ease-in-out, stroke 0.3s ease-in-out;
  fill: ${p => {
    if (p.fill === EnumIconButtonFill.default) {
      return p.theme.app.buttons.iconButton.colors[EnumIconButtonFill.default].fill
    }
  }};
  stroke: ${p => {
    if (p.stroke === EnumIconButtonFill.default) {
      return p.theme.app.buttons.iconButton.colors[EnumIconButtonFill.default].fill
    }
  }};
  &:hover {
    fill: ${p => {
      if (p.fill === EnumIconButtonFill.default) {
        return p.theme.app.buttons.iconButton.colors[EnumIconButtonFill.default].hoverFill
      }
    }};
    stroke: ${p => {
      if (p.stroke === EnumIconButtonFill.default) {
        return p.theme.app.buttons.iconButton.colors[EnumIconButtonFill.default].fill
      }
    }};
  }
  &:focus {
    ${p => p.theme.app.accessibilityOutline}
  }
`

const IconButtonWrapperCircle = styled(IconButtonBaseWrapper)<{
  fill?: EnumIconButtonFill
  stroke?: EnumIconButtonFill
}>`
  border-radius: 100px;
  transition: border-color 0.3s ease-in-out;
  border: ${p => {
    const borderStr = (color: string) => `solid 1px ${color};`
    if (p.fill === EnumIconButtonFill.default || p.stroke === EnumIconButtonFill.default) {
      return borderStr(p.theme.app.buttons.iconButton.colors[EnumIconButtonFill.default].fill)
    }
  }};
  &:hover {
    border-color: ${p => {
      if (p.fill === EnumIconButtonFill.default || p.stroke === EnumIconButtonFill.default) {
        return p.theme.app.buttons.iconButton.colors[EnumIconButtonFill.default].hoverFill
      }
    }};
  }
`

const IconButton: React.FC<{
  onClick: () => void
  fill?: EnumIconButtonFill
  stroke?: EnumIconButtonFill
  size?: number
  circle?: boolean
}> = ({fill, stroke, onClick, children, size = 30, circle = false}) => {
  if (circle) {
    return (
      <IconButtonWrapperCircle fill={fill} stroke={stroke} onClick={onClick} size={size}>
        {children}
      </IconButtonWrapperCircle>
    )
  }
  return (
    <IconButtonBaseWrapper fill={fill} stroke={stroke} onClick={onClick} size={size}>
      {children}
    </IconButtonBaseWrapper>
  )
}

export default IconButton
