import styled from 'styled-components'
import {NavLink} from 'react-router-dom'
import sg from '../../../../style-guidelines'
import {TButtonSize, TButtonTheme} from '../types'

export const pillButtonBaseStyle = (
  size: TButtonSize,
  buttonTheme: TButtonTheme,
  disabled?: boolean,
  inline?: boolean,
  centerAlign?: boolean
): string => {
  return `
  ${sg.app.buttons.sizes[size].insetSquish}
  display: block;
  width: ${inline ? null : '100%'};
  background: ${sg.app.buttons.pillButton.colors[buttonTheme].background};
  border:none;
  border-radius: 4px;
  color: white;
  cursor: ${disabled ? 'not-allowed' : 'pointer'};
  display: flex;
  align-items: center;

  ${centerAlign && 'justify-content: center;'}

  font-weight: 900;
  font-size: 14px;
  transition: background 0.3s ease-in-out;

  &:hover {
    background: ${sg.app.buttons.pillButton.colors[buttonTheme].hoverBackground};
  }
`
}

export const pillButtonActiveStyle = (buttonTheme: TButtonTheme): string => `
  background: ${sg.app.buttons.pillButton.colors[buttonTheme].activeBackground};
`

export const ButtonWrapper = styled.button<{
  size: TButtonSize
  buttonTheme: TButtonTheme
  disabled?: boolean
  active?: boolean
  inline?: boolean
  centerAlign?: boolean
}>`
  ${p => pillButtonBaseStyle(p.size, p.buttonTheme, p.disabled, p.inline, p.centerAlign)}
  ${p => p.active && pillButtonActiveStyle(p.theme)}
`

const ACTIVE_CLASS_NAME = 'nav-item-active'

export const ButtonWrapperAsNavLink = styled(NavLink).attrs({activeClassName: ACTIVE_CLASS_NAME})<{
  size: TButtonSize
  buttontheme: TButtonTheme // lowercase here as these props are passed to native DOM anchor
  disabled?: boolean
  inline?: boolean
  centerAlign?: boolean
}>`
  ${p => pillButtonBaseStyle(p.size, p.buttontheme, p.disabled, p.inline, p.centerAlign)}
  &.${ACTIVE_CLASS_NAME} {
    ${p => pillButtonActiveStyle(p.buttontheme)}
  }
`
