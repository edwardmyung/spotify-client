import React from 'react'
import {ButtonWrapper, ButtonWrapperAsNavLink} from './styled'
import {TButtonSize, TButtonTheme} from '../types'

interface IPillButtonProps {
  size: TButtonSize
  buttonTheme?: TButtonTheme.primary | TButtonTheme.brand
  active?: boolean
  onClick?: () => void
  disabled?: boolean
  inline?: boolean
  centerAlign?: boolean
}

interface IPillButtonAsNavLinkProps {
  size: TButtonSize
  buttonTheme?: TButtonTheme.primary
  to: string
  disabled?: boolean
  inline?: boolean
  exact?: boolean
  centerAlign?: boolean
}

const PillButton: React.FC<IPillButtonProps> = ({children, buttonTheme = TButtonTheme.primary, ...props}) => {
  return (
    <ButtonWrapper buttonTheme={buttonTheme} {...props}>
      {children}
    </ButtonWrapper>
  )
}

export const PillButtonAsNavLink: React.FC<IPillButtonAsNavLinkProps> = ({
  children,
  buttonTheme = TButtonTheme.primary,
  ...props
}) => {
  return (
    <ButtonWrapperAsNavLink buttontheme={buttonTheme} {...props}>
      {children}
    </ButtonWrapperAsNavLink>
  )
}

export default PillButton
