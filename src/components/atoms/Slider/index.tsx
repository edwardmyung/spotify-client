import React, {useRef} from 'react'
import styled from 'styled-components'
import colors from '../../../style-guidelines/constants/colors'

const barStyles = p => `
  width: 100%;
  height: 4px;
  border-radius: 100px;
  outline: none;
  margin: 0px;

  background: ${(() => {
    const value = ((p.value - p.min) / (p.max - p.min)) * 100
    return `linear-gradient(to right, ${colors.grays[30]} 0%, ${colors.grays[30]} ${value}%, ${colors.grays[70]} ${value}%, ${colors.grays[70]} 100%)`
  })()};
  
  &:hover { 
    background: ${(() => {
      const value = ((p.value - p.min) / (p.max - p.min)) * 100
      return `linear-gradient(to right, ${colors.brand.main} 0%, ${
        colors.brand.main
      } ${value}%, ${'grey'} ${value}%, ${'grey'} 100%)`
    })()};
    &::-webkit-slider-thumb {
      display:block;
      background: ${colors.white};
    }
    &::-moz-range-thumb {
      display:block;
      background:${colors.white};
    }
  }
`

const thumbStyles = props => `
  width: 15px;
  height: 15px;
  border-radius: 15px;
  background: ${props.color};
  cursor: pointer;
  -webkit-transition: .2s;
  transition: opacity .2s;
  display: none;
`

const Styles = styled.div<{min: number; max: number; value: number}>`
  display: flex;
  align-items: center;
  .slider {
    -webkit-appearance: none;
    ${p => barStyles(p)}
    &::-webkit-slider-thumb {
      -webkit-appearance: none;
      appearance: none;
      ${props => thumbStyles(props)}
    }
    &::-moz-range-thumb {
      ${props => thumbStyles(props)}
    }

    /* 
      Very useful post on styling range input
      https://stackoverflow.com/questions/18389224/how-to-style-html5-range-input-to-have-different-color-before-and-after-slider
    */
  }
`

interface IProps {
  width
  value: number | null
  onChange: (e: React.SyntheticEvent) => void
  min?: number
  max?: number
  step?: number
  style?: React.CSSProperties
}

const Slider: React.FC<IProps> = ({
  width = 100,
  min = 0,
  max = 100,
  step = 1,
  value = 0,
  onChange,
  style = {}
}): JSX.Element => {
  const rangeInputRef = useRef(null)

  return (
    <div style={{width, ...style}}>
      <Styles min={min} max={max} value={value}>
        <input
          type='range'
          min={min}
          max={max}
          value={value}
          step={step}
          className='slider'
          onChange={onChange}
          ref={rangeInputRef}
        />
      </Styles>
    </div>
  )
}

export default Slider
