import React, {useRef, useEffect} from 'react'

const ClickOutsideDetector: React.FC<{callback: () => void}> = ({children, callback}) => {
  const wrapperRef = useRef(null)

  const handleOutsideClick = e => {
    if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
      callback()
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleOutsideClick)
    return () => {
      document.removeEventListener('click', handleOutsideClick)
    }
  }, [])

  return <div ref={wrapperRef}>{children}</div>
}

export default ClickOutsideDetector
