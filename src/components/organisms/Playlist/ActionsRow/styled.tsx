import styled from 'styled-components'

export const Wrapper = styled.div`
  ${p => p.theme.constants.space.css.inset.l}
  padding-bottom: 0px;
  display: flex;
  align-items: center;
  background-image: linear-gradient(
    to bottom,
    ${p => p.theme.constants.colors.grays[70]},
    ${p => p.theme.constants.colors.grays[95]}
  );
`

export const IconButtonWrapper = styled.div<{size: number}>`
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  display: flex;
  align-items: center;
  justify-content: center;
`
