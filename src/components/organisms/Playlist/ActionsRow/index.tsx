import React from 'react'
import {Wrapper, IconButtonWrapper} from './styled'
import ControlPlay from '../../../atoms/icons/ControlPlay'
import CircleButton from '../../../atoms/buttons/CircleButton'
import {TButtonTheme} from '../../../atoms/buttons/types'
import IconButton, {EnumIconButtonFill} from '../../../atoms/buttons/IconButton'
import HeartOutline from '../../../atoms/icons/HeartOutline'

const PlaylistActionsRow: React.FC = () => {
  return (
    <Wrapper>
      <CircleButton
        buttonTheme={TButtonTheme.brand}
        onClick={() => console.log(123)}
        size={60}
        content={<ControlPlay fill={'white'} size={40} />}
      />
      <IconButtonWrapper size={60}>
        <IconButton onClick={() => alert('heart!')} stroke={EnumIconButtonFill.default}>
          <HeartOutline stroke={EnumIconButtonFill.delegate} size={40} />
        </IconButton>
      </IconButtonWrapper>
    </Wrapper>
  )
}

export default PlaylistActionsRow
