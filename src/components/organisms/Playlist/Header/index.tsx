import React from 'react'
import {Wrapper, MetaWrapper, Label, Name, Creator, Image} from './styled'

const PlaylistHeader: React.FC<{
  image: Record<string, unknown>
  playlistName: string
  creator: Record<string, unknown>
}> = ({image, playlistName, creator}) => {
  return (
    <Wrapper>
      <div>{image ? <Image src={image.url as string} style={{width: 200}} /> : <h1>NO IMAGE</h1>}</div>
      <MetaWrapper>
        <Label>PLAYLIST</Label>
        <Name>{playlistName}</Name>
        <Creator>{creator.display_name}</Creator>
      </MetaWrapper>
    </Wrapper>
  )
}

export default PlaylistHeader
