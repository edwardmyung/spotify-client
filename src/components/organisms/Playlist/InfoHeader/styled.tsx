import styled from 'styled-components'
import {FontWeight} from '../../../../style-guidelines/constants/typography'

const HEADER_HEIGHT = 60

export const Wrapper = styled.div`
  ${p => p.theme.constants.space.css.inset.l}
  padding-top: ${p => p.theme.constants.space.scale.thirtyTwo + HEADER_HEIGHT}px;
  margin-top: -${HEADER_HEIGHT}px;
  display: flex;
  color: white;
  align-items: flex-end;
  background: #dd5e89;
  background: -webkit-linear-gradient(to right, #f7bb97, #dd5e89);
  background: linear-gradient(to right, #f7bb97, #dd5e89);
`

export const MetaWrapper = styled.div`
  ${p => p.theme.constants.space.css.inline.left.l}
`

export const Label = styled.div`
  ${p => p.theme.constants.space.css.stack.m}
  ${p => p.theme.constants.typography.css.minion(FontWeight.bold)}
  letter-spacing: 3px;
`

export const Name = styled.div`
  ${p => p.theme.constants.typography.css.trafalgar(FontWeight.bold)}
  ${p => p.theme.constants.space.css.stack.s}
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
`

export const Creator = styled.div``

export const Image = styled.img`
  border-width: 0;
  box-shadow: rgba(0, 0, 0, 0.5) 0 4px 60px;
  box-sizing: border-box;
  user-select: none;
`
