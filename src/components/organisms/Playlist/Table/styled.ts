import styled from 'styled-components'
import {FontWeight} from '../../../../style-guidelines/constants/typography'
import {GridTableCellOverflowClip} from '../../../molecules/table'

export const Wrapper = styled.div`
  ${p => p.theme.constants.space.css.inset.l}
  background: ${p => p.theme.constants.colors.grays[95]};
  flex-grow: 1;
  color: white;
`

const IxGridTableCellBase = styled.div`
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const IxGridTableCellPlayIcon = styled(IxGridTableCellBase)`
  /* background: blue; */
  text-align: center;
`

export const IxGridTableCellNumber = styled(IxGridTableCellBase)`
  /* background: greenyellow; */
  text-align: center;
`

export const TrackName = styled(GridTableCellOverflowClip)`
  ${p => p.theme.constants.typography.css.longPrimer(FontWeight.regular)}
  ${p => p.theme.constants.space.css.stack.xs}
`

export const TrackArtists = styled(GridTableCellOverflowClip)`
  ${p => p.theme.constants.typography.css.brevier(FontWeight.regular)}
`

export const TrackAlbum = styled(GridTableCellOverflowClip)`
  ${p => p.theme.constants.typography.css.brevier(FontWeight.regular)}
`

export const TrackDuration = styled.div`
  ${p => p.theme.constants.typography.css.brevier(FontWeight.regular)}
`
