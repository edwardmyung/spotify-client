import React, {useState} from 'react'
import {
  Wrapper,
  TrackName,
  TrackArtists,
  TrackAlbum,
  TrackDuration,
  IxGridTableCellPlayIcon,
  IxGridTableCellNumber
} from './styled'
import {GridTableRow, GridTableCell, GridTableHeaderCell, GridTableHr} from '../../../molecules/table'
import {useStore} from '../../../../context/store'
import ClickOutsideDetector from '../../../utils/ClickOutsideDetector'
import {observer} from 'mobx-react'
import {ITrack} from '../../../../mobx/TrackStore/types'
import moment from 'moment'
import {playerRemote} from '../../Player'

const TableHeader = () => {
  return (
    <GridTableRow>
      <GridTableHeaderCell style={{textAlign: 'center'}}>#</GridTableHeaderCell>
      <GridTableHeaderCell>Title</GridTableHeaderCell>
      <GridTableHeaderCell>Album</GridTableHeaderCell>
      <GridTableHeaderCell>🕔</GridTableHeaderCell>
    </GridTableRow>
  )
}

const durationFromMs = (durationInMs: number) => {
  const durationMoment = moment.duration(durationInMs)

  const minutes = Math.floor(durationMoment.asMinutes())
  const seconds = durationMoment.seconds()
  const secondsStr = `${seconds < 10 ? '0' : ''}${seconds}`

  return `${minutes}:${secondsStr}`
}

const IxGridTableCell: React.FC<{
  number: number
  commonCellProps: {onDoubleClick: () => void}
  isPlaying: boolean
  isHovered: boolean
  pauseTrack: () => void
}> = ({number, commonCellProps, isPlaying, isHovered, pauseTrack}) => {
  let body

  if (isPlaying) {
    if (isHovered) {
      body = <IxGridTableCellPlayIcon onClick={() => pauseTrack()}>⏸</IxGridTableCellPlayIcon>
    } else {
      body = <IxGridTableCellPlayIcon>🎸</IxGridTableCellPlayIcon>
    }
  } else {
    if (isHovered) {
      body = <IxGridTableCellPlayIcon onClick={() => console.log('set is playing to true')}>▶️</IxGridTableCellPlayIcon>
    } else {
      body = <IxGridTableCellNumber>{number}</IxGridTableCellNumber>
    }
  }

  return <GridTableCell {...commonCellProps}>{body}</GridTableCell>
}

const Row: React.FC<{
  ix: number
  track: ITrack
  startTrack: () => void
  pauseTrack: () => void
  selectedRowIx: null | number
  setSelectedRowIx: (ix: number) => void
  isPlaying: boolean
  isSelectedInSpotifyPlayer: boolean
}> = ({ix, track, startTrack, pauseTrack, selectedRowIx, setSelectedRowIx, isPlaying, isSelectedInSpotifyPlayer}) => {
  const [isHovered, setIsHovered] = useState(false)

  const commonCellProps = {
    onDoubleClick: () => startTrack()
  }

  return (
    <GridTableRow
      onClick={() => setSelectedRowIx(ix)}
      isSelectable
      isSelected={selectedRowIx === ix}
      onMouseOver={() => setIsHovered(true)}
      onMouseOut={() => setIsHovered(false)}>
      <IxGridTableCell
        commonCellProps={commonCellProps}
        number={ix + 1}
        isPlaying={isPlaying && isSelectedInSpotifyPlayer}
        isHovered={isHovered}
        pauseTrack={pauseTrack}
      />
      <GridTableCell {...commonCellProps} style={{flexDirection: 'column'}}>
        <TrackName>{track.name}</TrackName>
        <TrackArtists>
          {track.artists.map(artist => (
            <span key={artist.id}>{artist.name}</span>
          ))}
        </TrackArtists>
      </GridTableCell>
      <GridTableCell {...commonCellProps}>
        <TrackAlbum>{track.album.name}</TrackAlbum>
      </GridTableCell>
      <GridTableCell {...commonCellProps}>
        <TrackDuration>{durationFromMs(track.duration_ms)}</TrackDuration>
      </GridTableCell>
    </GridTableRow>
  )
}

const PlaylistTable = observer(({playlist}) => {
  const store = useStore()
  const [selectedRowIx, setSelectedRowIx] = useState<null | number>(null)
  return (
    <Wrapper>
      <TableHeader />
      <GridTableHr />
      <ClickOutsideDetector callback={() => setSelectedRowIx(null)}>
        {playlist.tracks.items?.map((tr: ITrack, ix) => {
          return (
            <Row
              key={ix}
              ix={ix}
              track={tr}
              startTrack={() => {
                const track = store.track.tracks[tr.id]
                playerRemote.playTrack(
                  store.ui.userAuth.access_token,
                  store.ui.playerState.deviceId,
                  track.uri,
                  playlist.uri
                )
              }}
              pauseTrack={() => {
                playerRemote.pauseTrack(store.ui.userAuth.access_token, store.ui.playerState.deviceId)
              }}
              selectedRowIx={selectedRowIx}
              setSelectedRowIx={() => setSelectedRowIx(ix)}
              isPlaying={store.ui.playerState.isPlaying}
              isSelectedInSpotifyPlayer={store.ui.playerState.trackId === tr.id}
            />
          )
        })}
      </ClickOutsideDetector>
    </Wrapper>
  )
})

export default PlaylistTable
