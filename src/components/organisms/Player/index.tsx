import React, {useEffect, useState} from 'react'
import {observer} from 'mobx-react'
import {useStore} from '../../../context/store'
import PlayerTrackInfo from '../../molecules/player/TrackInfo'
import PlayerCentralControls from '../../molecules/player/CentralControls'
import PlayerPeripheralControls from '../../molecules/player/PeripheralControls'
import {Wrapper, SideChild, CentreChild} from './styled'
import {EnumPlayerStateCtxType} from '../../../mobx/UiStore/types'
import * as api from '../../../api'

declare global {
  interface Window {
    Spotify: {
      Player: any
    }
  }
}

const getIdFromUriString = (uriString: string) => {
  const parts = uriString.split(':')
  return parts[parts.length - 1]
}

export const playerRemote = {
  playTrack: (
    token: string,
    deviceId: string,
    trackUri: string | null = null,
    albumOrPlaylistUri: string | null = null,
    trackUris: string[] = []
  ): void => {
    let body
    if (trackUris.length > 0) {
      body = JSON.stringify({
        uris: trackUris
      })
    } else {
      body = JSON.stringify({
        context_uri: albumOrPlaylistUri,
        offset: {uri: trackUri}
      })
    }

    fetch(api.player.playTrack(token, deviceId, body))
  },
  pauseTrack: (token: string, deviceId: string): void => {
    fetch(api.player.pauseTrack(token, deviceId))
  }
}

const checkForPlayer = (store, token, setSpotifyPlayer, playerCheckInterval) => {
  const createEventHandlers = spotifyPlayer => {
    spotifyPlayer.on('initialization_error', e => console.error(e))
    spotifyPlayer.on('authentication_error', e => console.error(e))
    spotifyPlayer.on('account_error', e => console.error(e))
    spotifyPlayer.on('playback_error', e => console.error(e))

    spotifyPlayer.on('player_state_changed', state => {
      // Keep the mobx store in sync with any changes in the spotify player
      let mobxPlayerState
      if (state.context.uri.includes('playlist')) {
        mobxPlayerState = {
          type: EnumPlayerStateCtxType.playlist,
          trackId: state.track_window.current_track.linked_from.id || state.track_window.current_track.id,
          playlistId: getIdFromUriString(state.context.uri)
        }
      } else if (state.context.uri.includes('album')) {
        mobxPlayerState = {
          type: EnumPlayerStateCtxType.album,
          trackId: state.track_window.current_track.id,
          albumId: getIdFromUriString(state.context.uri)
        }
      }
      store.ui.setPlayerState({
        isPlaying: !state.paused,
        duration: state.duration,
        position: state.position,
        ...mobxPlayerState
      })
    })

    spotifyPlayer.on('ready', ({device_id}) => {
      console.log('Spotify player successfully connected, on device: ', device_id)
      store.ui.setPlayerState({
        deviceId: device_id
      })
      spotifyPlayer.resume()
    })
  }

  if (window.Spotify !== null) {
    clearInterval(playerCheckInterval)

    const spotifyPlayer = new window.Spotify.Player({
      name: 'Spotify Web API Project',
      getOAuthToken: cb => cb(token)
    })

    createEventHandlers(spotifyPlayer)
    spotifyPlayer.connect()
    spotifyPlayer.getVolume().then(volume => {
      console.log(volume)
    })
    setSpotifyPlayer(spotifyPlayer)
  }
}

const Player: React.FC<{token: string}> = observer(
  ({token}): JSX.Element => {
    let playerCheckInterval
    const store = useStore()

    const [spotifyPlayer, setSpotifyPlayer] = useState(null)

    useEffect(() => {
      if (token) {
        playerCheckInterval = setInterval(
          () => checkForPlayer(store, token, setSpotifyPlayer, playerCheckInterval),
          1000
        )
      }
    }, [])

    const prevTrack = () => {
      spotifyPlayer.previousTrack()
      spotifyPlayer.resume()
    }

    const nextTrack = () => {
      spotifyPlayer.nextTrack()
      spotifyPlayer.resume()
    }

    const pauseTrack = () => {
      spotifyPlayer.pause()
    }

    const playTrack = () => {
      spotifyPlayer.resume()
    }

    const togglePlay = () => {
      spotifyPlayer.togglePlay()
    }

    const setVolume = vol => {
      spotifyPlayer.setVolume(vol)
    }

    const maybeTrack = store.track.tracks[store.ui.playerState.trackId]

    return (
      <Wrapper>
        <SideChild>
          <PlayerTrackInfo track={maybeTrack} artists={maybeTrack?.artists} />
        </SideChild>
        <CentreChild>
          <PlayerCentralControls
            isPlaying={store.ui.playerState.isPlaying}
            togglePlay={togglePlay}
            prevTrack={prevTrack}
            nextTrack={nextTrack}
          />
        </CentreChild>
        <SideChild>
          <PlayerPeripheralControls
            setPlayerVolume={setVolume}
            initialVolume={spotifyPlayer ? spotifyPlayer.getVolume() : 0}
          />
        </SideChild>
      </Wrapper>
    )
  }
)

export default Player
