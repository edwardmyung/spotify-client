import React from 'react'
import LogoIcon from '../../../atoms/icons/Logo'
import {Wrapper, ContentWrapper, ContentRow, LoginButton} from './styled'
import concertImg from '../../../../assets/img/concert.jpg'
import {colors} from '../../../../style-guidelines/constants'

const AUTH_ENDPOINT = 'https://accounts.spotify.com/authorize'
const CLIENT_ID = 'bb36610f3deb4c0383e50e4fb6866ea6'
const REDIRECT_URI = 'http://localhost:8080'
const SCOPES = [
  'playlist-read-collaborative',
  'playlist-modify-private',
  'playlist-modify-public',
  'playlist-read-private',
  'user-modify-playback-state',
  'user-read-currently-playing',
  'user-read-playback-state',
  'user-read-private',
  'user-read-email',
  'user-library-modify',
  'user-library-read',
  'user-follow-modify',
  'user-follow-read',
  'user-read-recently-played',
  'user-top-read',
  'streaming',
  'app-remote-control'
]

const AUTH_URL = `${AUTH_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&scope=${SCOPES.join(
  '%20'
)}&response_type=token&show_dialog=true`

const LoginBody: React.FC = () => {
  return (
    <Wrapper imgSrc={concertImg}>
      <ContentWrapper>
        {/* <ContentWrapperFrostedGlass /> */}
        <ContentRow style={{textAlign: 'center'}}>
          <LogoIcon
            width={0}
            fill={colors.brand.main}
            style={{width: '100%', maxWidth: 145, margin: '0px 20px'}}
            animated
          />
        </ContentRow>
        <ContentRow>Techniques: Design Systems, Atomic Design</ContentRow>
        <ContentRow>Technologies: React, MobX, Styled Components, StoryBook</ContentRow>
        <a href={AUTH_URL}>
          <LoginButton>Login with Spotify</LoginButton>
        </a>
      </ContentWrapper>
    </Wrapper>
  )
}

export default LoginBody
