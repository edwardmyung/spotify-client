import styled from 'styled-components'

export const Wrapper = styled.div<{
  imgSrc: string
}>`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-image: url(${p => p.imgSrc});
  background-position: center center;
  background-size: cover;

  display: flex;
  justify-content: center;
`

export const ContentWrapper = styled.div`
  background: rgba(255, 255, 255, 0.3);
  backdrop-filter: blur(8px);
  max-width: 300px;
  width: 100%;
  align-self: center;
  border-radius: 10px;
  overflow: hidden;
`

export const LoginButton = styled.div`
  ${p => p.theme.constants.space.css.insetSquish.m}
  background: ${p => p.theme.constants.colors.brand.main};
  text-align: center;
  color: white;
  &:hover {
    background: ${p => p.theme.constants.colors.brand.hover};
  }
`

export const ContentRow = styled.div`
  ${p => p.theme.constants.space.css.inset.m}
  ${p => p.theme.constants.typography.css.brevier}
  border-bottom: solid 1px rgba(255,255,255,0.2);
`

export const ContentWrapperFrostedGlass = styled.div``
