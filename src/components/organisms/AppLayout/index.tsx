import React from 'react'
import {GridContainer, GridAside, GridFooter} from '../../atoms/appLayout'
import AppLayoutSidebar from '../../molecules/appLayout/Sidebar'
import {IPlaylist} from '../../../mobx/PlaylistStore/types'
import Player from '../../organisms/Player'
interface IAppLayout {
  token: string
  body: JSX.Element
  fetchUserPlaylists: () => void
  userPlaylists: IPlaylist[]
}

const AppLayout: React.FC<IAppLayout> = ({token, body, userPlaylists, fetchUserPlaylists}) => {
  return (
    <GridContainer>
      <GridAside>
        <AppLayoutSidebar fetchUserPlaylists={fetchUserPlaylists} userPlaylists={userPlaylists} />
      </GridAside>
      <GridFooter>
        <Player token={token} />
      </GridFooter>
      {body}
    </GridContainer>
  )
}

export default AppLayout
