# About this project

This is a Spotify Clone built using: ReactJS, MobX, Typescript, Styled Components

The main aim of this project was to create a Design System to manage typography, space and color across a complex app. 

N.B. This is an experimetnal project to illustrate ideas, not a full implementation of Spotify's web client. i.e. Not all views are implemented, however the groundwork to enable a team to rapidly develop UIs with consistent design is by-and-large complete.

# Running the project

1. Clone the repo
2. Run `npm install`
3. Run `yarn start`
4. Auth with Spotify
5. Select a playlist from the side-menu